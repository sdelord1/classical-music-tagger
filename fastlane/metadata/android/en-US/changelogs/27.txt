- Handle denied permission in SAF mode, with appropriate error message.
- List SD card base directory with leading '/', like internal memory.
(v1.9.1)
