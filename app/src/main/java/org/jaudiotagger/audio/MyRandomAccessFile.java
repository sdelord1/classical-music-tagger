/*
 * Copyright (C) 2020-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.jaudiotagger.audio;

import java.io.IOException;
import java.nio.channels.FileChannel;


// https://stackoverflow.com/questions/28897329/documentfile-randomaccessfile

public interface MyRandomAccessFile extends AutoCloseable
{
    int read() throws IOException;

    int read(byte[] b);

    byte readByte();

    void readFully(byte[] b) throws IOException;

    void readFully (byte[] b, int off, int len) throws IOException;

    int read(byte[] b, int off, int len);

    void write(byte[] b, int off, int len);

    void write(byte[] b);

    void write(int b);

    int skipBytes(int n);

    void seek(long pos) throws IOException;

    void close();

    FileChannel getReadChannel();

    FileChannel getWriteChannel();

    FileChannel getChannel();

    long length();

    long getFilePointer();

    void setLength (long newLength);

    boolean isSeekable();

    boolean isValid();

    boolean isMemBased();
}
