/*
 * Copyright (C) 2020-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.jaudiotagger.audio;

import android.net.Uri;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Path;


public interface MyFile
{
    //
    // kind of constructors
    //

    /*
    public MyFile getAbsoluteFile()
    {
        return new MyFile(f.getAbsoluteFile());
    }
    */

    MyFile getParentFile();

    MyRandomAccessFile getRandomAccessFile(final String mode);

    /*
    public static MyFile createTempFile(String prefix, String suffix, MyFile directory)
    {
        try
        {
            return new MyFile(File.createTempFile(prefix, suffix, directory.f));
        } catch (IOException e)
        {
            return null;
        }
    }
     */

    MyFile createTempFileInSameDirectory(String suffix);

    MyFile createTempFile(String suffix);

    void objcopyFrom(MyFile from);

    //
    // names and paths
    //

    String getName();

    String getPath();

    Uri getUri();

    String getAbsolutePath();

    boolean exists();

    boolean delete();

    // boolean isHidden() { return f.isHidden(); }

    boolean isDirectory();

    boolean renameTo(String filename);

    boolean renameTo(MyFile newfile);

    boolean canRead();

    boolean canWrite();

    long length();

    long lastModified();     // get modification tiem

    long created();          // get creation time

    @SuppressWarnings("UnusedReturnValue")
    boolean setLastModified(long time);      // set modification time

    @SuppressWarnings("UnusedReturnValue")
    boolean setCreated(long time);           // set creation time

    FileInputStream getFileInputStream();

    FileOutputStream getFileOutputStream();

    @SuppressWarnings("unused")
    Path toPath();

    //
    // utility functions
    //

    boolean existsInSameDirectory(String name);

    String getBaseName();

    boolean replaceWith(MyFile newFile);
}
