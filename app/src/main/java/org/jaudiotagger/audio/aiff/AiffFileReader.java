package org.jaudiotagger.audio.aiff;

import org.jaudiotagger.audio.MyRandomAccessFile;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.generic.AudioFileReader;
import org.jaudiotagger.audio.generic.GenericAudioHeader;
import org.jaudiotagger.tag.Tag;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Reads Audio and Metadata information contained in Aiff file.
 */
public class AiffFileReader extends AudioFileReader
{
    @Override
    protected GenericAudioHeader getEncodingInfo(MyRandomAccessFile raf) throws CannotReadException, IOException
    {
        return new AiffInfoReader("UNKNOWN").read(raf);
    }

    @Override
    protected Tag getTag(MyRandomAccessFile raf) throws CannotReadException, IOException
    {
        return new AiffTagReader("UNKNOWN").read(raf);
    }
}
