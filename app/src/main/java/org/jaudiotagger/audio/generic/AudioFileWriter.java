/*
 * Entagged Audio Tag library
 * Copyright (c) 2003-2005 Raphaël Slinckx <raphael@slinckx.net>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package org.jaudiotagger.audio.generic;

import android.util.Log;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.MyFile;
import org.jaudiotagger.audio.MyRandomAccessFile;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.logging.ErrorMessage;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagOptionSingleton;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This abstract class is the skeleton for tag writers.
 * <p/>
 * <p/>
 * It handles the creation/closing of the randomaccessfile objects and then call
 * the subclass method writeTag or deleteTag. These two method have to be
 * implemented in the subclass.
 *
 * @author Raphael Slinckx
 * @version $Id: AudioFileWriter.java,v 1.21 2009/05/05 15:59:14 paultaylor Exp
 *          $
 * @since v0.02
 */
public abstract class AudioFileWriter
{
    protected final static String LOG_TAG = "JAT";
    private static final String WRITE_MODE = "rw";
    private static final int MINIMUM_FILESIZE = 100;

    // Logger Object
    public static Logger logger = Logger.getLogger("org.jaudiotagger.audio.generic");

    /**
     * Delete the tag (if any) present in the given file
     *
     * @param af The file to process
     * @throws CannotWriteException                                  if anything went wrong
     * @throws org.jaudiotagger.audio.exceptions.CannotReadException (missing description)
     */
    public void delete(AudioFile af) throws CannotReadException, CannotWriteException
    {
        MyFile file = af.getFile();
        if (TagOptionSingleton.getInstance().isCheckIsWritable() && !file.canWrite())
        {
            //logger.severe(Permissions.displayPermissions(file));
            throw new CannotWriteException(ErrorMessage.GENERAL_DELETE_FAILED.getMsg(file));
        }

        if (af.getFile().length() <= MINIMUM_FILESIZE)
        {
            throw new CannotWriteException(ErrorMessage.GENERAL_DELETE_FAILED_BECAUSE_FILE_IS_TOO_SMALL.getMsg(file));
        }

        MyFile tempF = file.createTempFileInSameDirectory(".tmp");
        if (tempF == null)
        {
            Log.w(LOG_TAG, "delete() : cannot create temp file in same directory, using general temp file");
            tempF = file.createTempFile(".tmp");
        }
        if (tempF == null)
        {
            throw new CannotWriteException(ErrorMessage.GENERAL_DELETE_FAILED.getMsg(file));
        }

        // Will be set to true on VetoException, causing the finally block to
        // discard the tempfile.
        boolean revert = false;

        MyRandomAccessFile raf = af.getFile().getRandomAccessFile(WRITE_MODE);
        if (raf == null)
        {
            throw new CannotReadException("file is not accessible");
        }
        if (!raf.isSeekable())
        {
            throw new CannotReadException("file is not seekable");
        }

        MyRandomAccessFile rafTemp = tempF.getRandomAccessFile(WRITE_MODE);
        if (rafTemp == null)
        {
            throw new CannotReadException("file is not accessible");
        }
        if (!rafTemp.isSeekable())
        {
            throw new CannotReadException("file is not seekable");
        }

        try
        {
            raf.seek(0);
            rafTemp.seek(0);
            deleteTag(af.getTag(), raf, rafTemp);
        }
        catch (Exception e)
        {
            revert = true;
            throw new CannotWriteException("\"" + af.getFile().getAbsolutePath() + "\" :" + e, e);
        }
        finally
        {
            // will be set to the remaining file.
            //MyFile result = af.getFile();
            try
            {
                if (raf != null)
                {
                    raf.close();
                }
                if (rafTemp != null)
                {
                    rafTemp.close();
                }

                if (tempF.length() > 0 && !revert)
                {
                    boolean deleteResult = af.getFile().delete();
                    if (!deleteResult)
                    {
                        logger.warning(ErrorMessage.GENERAL_WRITE_FAILED_TO_DELETE_ORIGINAL_FILE.getMsg(af.getFile().getPath(), tempF.getPath()));
                        throw new CannotWriteException(ErrorMessage.GENERAL_WRITE_FAILED_TO_DELETE_ORIGINAL_FILE.getMsg(af.getFile().getPath(), tempF.getPath()));
                    }
                    boolean renameResult = tempF.renameTo(af.getFile());
                    if (!renameResult)
                    {
                        logger.warning(ErrorMessage.GENERAL_WRITE_FAILED_TO_RENAME_TO_ORIGINAL_FILE.getMsg(af.getFile().getPath(), tempF.getPath()));
                        throw new CannotWriteException(ErrorMessage.GENERAL_WRITE_FAILED_TO_RENAME_TO_ORIGINAL_FILE.getMsg(af.getFile().getPath(), tempF.getPath()));
                    }
                    //result = tempF;

                    // If still exists we can now delete
                    if (tempF.exists())
                    {
                        if (!tempF.delete())
                        {
                            // Non critical failed deletion
                            logger.warning(ErrorMessage.GENERAL_WRITE_FAILED_TO_DELETE_TEMPORARY_FILE.getMsg(tempF.getPath()));
                        }
                    }
                }
                else
                {
                    // It was created but never used
                    if (!tempF.delete())
                    {
                        // Non critical failed deletion
                        logger.warning(ErrorMessage.GENERAL_WRITE_FAILED_TO_DELETE_TEMPORARY_FILE.getMsg(tempF.getPath()));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.severe("AudioFileWriter exception cleaning up delete:" + af.getFile().getPath() + " or" + tempF.getAbsolutePath() + ":" + ex);
            }
        }
    }

    /**
     * Delete the tag (if any) present in the given randomaccessfile, and do not
     * close it at the end.
     *
     * @param tag (no description)
     * @param raf     The source file, already opened in r-write mode
     * @param tempRaf The temporary file opened in r-write mode
     * @throws CannotWriteException                                  if anything went wrong
     * @throws org.jaudiotagger.audio.exceptions.CannotReadException (no description)
     * @throws java.io.IOException (no description)
     */
    public void delete(Tag tag, MyRandomAccessFile raf, MyRandomAccessFile tempRaf) throws CannotReadException, CannotWriteException, IOException
    {
        raf.seek(0);
        tempRaf.seek(0);
        deleteTag(tag, raf, tempRaf);
    }

    /**
     * Same as above, but delete tag in the file.
     *
     * @param tag (no description)
     * @param raf (no description)
     * @param tempRaf (no description)
     * @throws IOException                                           is thrown when the RandomAccessFile operations throw it (you
     *                                                               should never throw them manually)
     * @throws CannotWriteException                                  when an error occured during the deletion of the tag
     * @throws org.jaudiotagger.audio.exceptions.CannotReadException (no description)
     */
    protected abstract void deleteTag(Tag tag, MyRandomAccessFile raf, MyRandomAccessFile tempRaf) throws CannotReadException, CannotWriteException, IOException;

    /**
     * Prechecks before normal write
     * <p/>
     * <ul>
     * <li>If the tag is actually empty, remove the tag</li>
     * <li>if the file is not writable, throw exception
     * <li>
     * <li>If the file is too small to be a valid file, throw exception
     * <li>
     * </ul>
     *
     * @param af (no description)
     * @throws CannotWriteException (no description)
     */
    private void precheckWrite(AudioFile af) throws CannotWriteException
    {
        // Preliminary checks
        try
        {
            if (af.getTag().isEmpty())
            {
                delete(af);
                return;
            }
        }
        catch (CannotReadException re)
        {
            throw new CannotWriteException(ErrorMessage.GENERAL_WRITE_FAILED.getMsg(af.getFile().getPath()));
        }

        MyFile file = af.getFile();
        if (TagOptionSingleton.getInstance().isCheckIsWritable() && !file.canWrite())
        {
            //logger.severe(Permissions.displayPermissions(file));
            //logger.severe(ErrorMessage.GENERAL_WRITE_FAILED.getMsg(af.getFile().getPath()));
            throw new CannotWriteException(ErrorMessage.GENERAL_WRITE_FAILED_TO_OPEN_FILE_FOR_EDITING.getMsg(file));
        }

        if (af.getFile().length() <= MINIMUM_FILESIZE)
        {
            logger.severe(ErrorMessage.GENERAL_WRITE_FAILED_BECAUSE_FILE_IS_TOO_SMALL.getMsg(file));
            throw new CannotWriteException(ErrorMessage.GENERAL_WRITE_FAILED_BECAUSE_FILE_IS_TOO_SMALL.getMsg(file));
        }
    }

    /**
     * Write the tag (if not empty) present in the AudioFile in the associated
     * File
     *
     * @param af The file we want to process
     * @throws CannotWriteException if anything went wrong
     */
    // TODO Creates temp file in same folder as the original file, this is safe
    // but would impose a performance overhead if the original file is on a networked drive
    public void write(AudioFile af) throws CannotWriteException
    {
        MyFile file = af.getFile();
        logger.config("Started writing tag data for file:" + af.getFile().getName());

        // Prechecks
        precheckWrite(af);

        //mp3's use a different mechanism to the other formats
        if (af instanceof MP3File)
        {
            af.commit();
            return;
        }

        //
        // Open actual file for editing.
        // Note that internally this could be memory based in case the file
        // is not seekable or has no r/w access
        //

        MyRandomAccessFile raf = af.getFile().getRandomAccessFile(WRITE_MODE);
        if (raf == null)
        {
            throw new CannotWriteException("file is not accessible");
        }
        if (!raf.isSeekable())
        {
            throw new CannotWriteException("file is not seekable");
        }

        //
        // Create temporary file in same directory (preferred) or elsewhere
        // Note that tmp file must allow r/w access and seek mode
        //

        MyFile tempF = null;
        if (!raf.isMemBased())
        {
            // we do not try this is case the original file is memory buffered
            tempF = file.createTempFileInSameDirectory(".tmp");
        }
        if (tempF == null)
        {
            Log.w(LOG_TAG, "write() : cannot create temp file in same directory, using general temp file");
            tempF = file.createTempFile(".tmp");
        }
        if (tempF == null)
        {
            throw new CannotWriteException(ErrorMessage.GENERAL_WRITE_FAILED.getMsg(file));
        }

        MyRandomAccessFile rafTemp = tempF.getRandomAccessFile(WRITE_MODE);
        if (rafTemp == null)
        {
            throw new CannotWriteException("file is not accessible");
        }
        if (!rafTemp.isSeekable())
        {
            throw new CannotWriteException("file is not seekable");
        }

        // Write data to File
        try
        {
            raf.seek(0);
            rafTemp.seek(0);
            writeTag(af, af.getTag(), raf, rafTemp);
        }
        catch (Exception e)
        {
            logger.log(Level.SEVERE, ErrorMessage.GENERAL_WRITE_FAILED_BECAUSE.getMsg(af.getFile(), e.getMessage()), e);

            if (raf != null)
            {
                raf.close();
            }
            if (rafTemp != null)
            {
                rafTemp.close();
            }

            // Delete the temporary file because either it was never used so
            // lets just tidy up or we did start writing to it but
            // the write failed and we havent renamed it back to the original
            // file so we can just delete it.
            if (!tempF.delete())
            {
                // Non critical failed deletion
                logger.warning(ErrorMessage.GENERAL_WRITE_FAILED_TO_DELETE_TEMPORARY_FILE.getMsg(tempF.getAbsolutePath()));
            }
            throw new CannotWriteException(ErrorMessage.GENERAL_WRITE_FAILED_BECAUSE.getMsg(af.getFile(), e.getMessage()));
        }
        finally
        {
            if (raf != null)
            {
                raf.close();
            }
            if (rafTemp != null)
            {
                rafTemp.close();
            }
        }

        // Result held in this file
        //MyFile result = af.getFile();

        // If the temporary file was used
        if (tempF.length() > 0)
        {
            Log.d("CMT", "### temp file has size > 0");
            //transferNewFileToOriginalFile(newFile, af.getFile(), TagOptionSingleton.getInstance().isPreserveFileIdentity());
            boolean bRes = transferNewFileToNewOriginalFile(tempF, af.getFile());
            if (bRes)
            {
                af.setFile(tempF);
            }
        }
        else
        {
            // Delete the temporary file that wasn't ever used
            Log.d("CMT", "### temp file has size 0");
            if (!tempF.delete())
            {
                // Non critical failed deletion
                logger.warning(ErrorMessage.GENERAL_WRITE_FAILED_TO_DELETE_TEMPORARY_FILE.getMsg(tempF.getPath()));
            }
        }
    }


    /**
     * <p>
     * Replaces the original file with the new file in a way that changes the file identity.
     * In other words, the Unix inode or the Windows
     * <a href="https://msdn.microsoft.com/en-us/library/aa363788(v=vs.85).aspx">fileIndex</a>
     * of the resulting file with the name {@code originalFile} is not identical to the inode/fileIndex
     * of the file named {@code originalFile} before this method was called.
     * </p>
     * <p>
     * If no errors occur, the method follows this approach:
     * </p>
     * <ol>
     * <li>Rename <code>originalFile</code> to <code>originalFile.old</code></li>
     * <li>Rename <code>newFile</code> to <code>originalFile</code> (this implies a file identity change for <code>originalFile</code>)</li>
     * <li>Delete <code>originalFile.old</code></li>
     * <li>Delete <code>newFile</code></li>
     * </ol>
     *
     * @param newFile      File containing the data we want in the {@code originalFile}
     * @param originalFile Before execution this denotes the original, unmodified file.
     *                     After execution it denotes the name of the file with the modified content and new inode/fileIndex.
     */
    private boolean transferNewFileToNewOriginalFile(final MyFile newFile, final MyFile originalFile)
    {
        // get original creation date
        long creationTime = originalFile.created();

        // replace file
        boolean bRes = originalFile.replaceWith(newFile);

        // now also set the creation date to the creation date of the original file
        if (!bRes && (creationTime != 0))
        {
            // this may fail silently on OS X, because of a JDK bug
            newFile.setCreated(creationTime);
        }

        return bRes;
    }


    /**
     * This is called when a tag has to be written in a file. Three parameters
     * are provided, the tag to write (not empty) Two randomaccessfiles, the
     * first points to the file where we want to write the given tag, and the
     * second is an empty temporary file that can be used if e.g. the file has
     * to be bigger than the original.
     * <p/>
     * If something has been written in the temporary file, when this method
     * returns, the original file is deleted, and the temporary file is renamed
     * the the original name
     * <p/>
     * If nothing has been written to it, it is simply deleted.
     * <p/>
     * This method can assume the raf, rafTemp are pointing to the first byte of
     * the file. The subclass must not close these two files when the method
     * returns.
     *
     * @param audioFile (missing description)
     * @param tag (missing description)
     * @param raf (missing description)
     * @param rafTemp (missing description)
     * @throws IOException                                           is thrown when the RandomAccessFile operations throw it (you
     *                                                               should never throw them manually)
     * @throws CannotWriteException                                  when an error occured during the generation of the tag
     * @throws org.jaudiotagger.audio.exceptions.CannotReadException (missing description)
     */
    protected abstract void writeTag(
            AudioFile audioFile,
            Tag tag,
            MyRandomAccessFile raf,
            MyRandomAccessFile rafTemp)
           throws CannotReadException, CannotWriteException, IOException;
}
