/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;

import java.util.List;

/**
 * helper class to handle directories
 * @noinspection CommentedOutCode, RedundantSuppression
 */

public interface DirectoryTree
{
    interface DirectoryEntry extends Comparable<DirectoryEntry>
    {
        enum opResult
        {
            YES, NO, ERROR
        }
        int compareTo(DirectoryEntry o);
        @NonNull String toString();
        boolean isDirectory();
        String getName();
        String getPath();
        String getFilePath();
        @NonNull DirectoryEntry getParent();
        Uri getUri();
        boolean exists();
        boolean delete();
        boolean canRead();
        @SuppressWarnings({"unused", "RedundantSuppression"})
        boolean canWrite();
        boolean copyTo(DirectoryEntry destination, ContentResolver theContentResolver);
        DirectoryEntry copyTo(String name, ContentResolver theContentResolver);
        Object getFile();
        opResult existsInSameDirectory(String filename);
        //boolean renameTo(String name);
        boolean renameToEx(String name, boolean changeTo);
        opResult isSeekable(ContentResolver contentResolver);
    }

    //void setRoot(Context context, String base);
    //void setRoot(DirectoryEntry base);
    boolean isValid();
    boolean isRoot();
    DirectoryEntry getCurrent();
    String getCurrentUriAsString();
    DirectoryEntry gotoParent();
    DirectoryTree setCurrent(Context context, DirectoryEntry currentDir);
    void setCurrent(Context context, String currentDir);
    List<DirectoryEntry> getChildren();
    @NonNull String toString();
    @NonNull String getInfoString();
}
