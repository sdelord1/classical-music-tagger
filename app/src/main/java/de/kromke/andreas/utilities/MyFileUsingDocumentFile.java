/*
 * Copyright (C) 2020-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import androidx.documentfile.provider.DocumentFile;
import android.util.Log;
import android.webkit.MimeTypeMap;

import org.jaudiotagger.audio.MyFile;
import org.jaudiotagger.audio.MyRandomAccessFile;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/** @noinspection CommentedOutCode*/
public class MyFileUsingDocumentFile implements MyFile
{
    private DocumentFile f;
    private ContentResolver contentResolver;
    private static final String LOG_TAG = "CMT : MyFileUsDocFile";

    //
    // constructors
    //

    /*
    public MyFile(String path)
    {
        Log.d(LOG_TAG, "MyFile(path = \"" + path + "\")");
        f = new File(path);
    }
    */

    MyFileUsingDocumentFile(DocumentFile file, ContentResolver theContentResolver)
    {
        Log.d(LOG_TAG, "MyFile(file)");
        f = file;
        contentResolver = theContentResolver;
    }

    /*
    public MyFile(MyFile parent, String child)
    {
        Log.d(LOG_TAG, "MyFile(parent, child)");
        f = new File(parent.f, child);
    }
    */

    /*
    public MyFile(String parent, String child)
    {
        Log.d(LOG_TAG, "MyFile(parent, child)");
        f = new File(parent, child);
    }
    */

    //
    // kind of constructors
    //

    /*
    public MyFile getAbsoluteFile()
    {
        return new MyFile(f.getAbsoluteFile());
    }
    */

    public MyFileUsingDocumentFile getParentFile()
    {
        return new MyFileUsingDocumentFile(f.getParentFile(), contentResolver);
    }

    // access, especially rw, may fail, so prepare to return null here
    public MyRandomAccessFile getRandomAccessFile(final String mode)
    {
        // We cannot use the try-with-resources method, because this would
        // automatically close 'raf', but we must return an opened one.
        //noinspection resource
        MyRandomAccessFile raf = new MyRandomAccessFileUsingDocumentFile(f, mode, contentResolver);
        if (!raf.isValid() || !raf.isSeekable())
        {
            // fallback solution: use buffered, memory based file
            Log.d(LOG_TAG, "getRandomAccessFile() : trying fallback to memory based I/O");
            // We cannot use the try-with-resources method, because this would
            // automatically close 'raf', but we must return an opened one.
            //noinspection resource
            raf = new MyRandomAccessFileUsingMemory(f, mode, contentResolver);
        }
        return (raf.isValid()) ? raf : null;
    }

    /*
    public static MyFile createTempFile(String prefix, String suffix, MyFile directory)
    {
        try
        {
            return new MyFile(File.createTempFile(prefix, suffix, directory.f));
        } catch (IOException e)
        {
            return null;
        }
    }
     */

    public MyFileUsingDocumentFile createTempFileInSameDirectory(String suffix)
    {
        // get original name without extension
        String originalBaseName = getBaseName();

        // check for possible temp filename in same directory
        int count = 0;
        String tmpName = originalBaseName + suffix;
        while (existsInSameDirectory(tmpName))
        {
            count++;
            tmpName = originalBaseName + suffix + count;
        }

        DocumentFile parentFile = f.getParentFile();
        if (parentFile != null)
        {
            String extension = tmpName;
            int pos = tmpName.lastIndexOf('.');
            if (pos >= 0)
            {
                extension = tmpName.substring(pos + 1);
            }
            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            if (mime == null)
            {
                mime = "";
            }
            DocumentFile tmpFile = parentFile.createFile(mime, tmpName);
            return new MyFileUsingDocumentFile(tmpFile, contentResolver);
        }

        return null;
    }

    public MyFile createTempFile(String suffix)
    {
        // create dummy File
        String name = f.getName();
        if (name == null)
        {
            name = "dummy";
        }
        File ff = new File("/tmp/", name);
        // create dummy MyFile
        MyFile mf = new MyFileUsingFile(ff);
        // let our sister class do the work
        return mf.createTempFile(suffix);
    }

    public void objcopyFrom(MyFile from)
    {
        f = ((MyFileUsingDocumentFile) from).f;
        contentResolver = ((MyFileUsingDocumentFile) from).contentResolver;
    }

    //
    // names and paths
    //

    public String getName()
    {
        return f.getName();
    }

    public String getPath()
    {
        return (f == null) ? null : f.getUri().getPath();
    }

    public String getAbsolutePath()
    {
        return f.getUri().getPath();
    }

    public boolean exists()
    {
        return f.exists();
    }

    public boolean delete() { return f.delete(); }

    //public boolean isHidden() { return f.isHidden(); }

    public boolean isDirectory() { return f.isDirectory(); }

    public boolean renameTo(String filename)
    {
        return f.renameTo(filename);
    }

    public boolean renameTo(MyFile newfile)
    {
        return f.renameTo(newfile.getName());
    }

    public boolean canRead()
    {
        // Due to an Android or Android documentation bug the return code
        // of canRead() is always "false" in case it's not a file, but however
        // one can get an InputStream.
        return (!f.isFile()) || (f.canRead());
    }

    public boolean canWrite()
    {
        return f.canWrite();
    }

    public long length()
    {
        return f.length();
    }

    public long lastModified()
    {
        return f.lastModified();
    }

    // https://stackoverflow.com/questions/35744654/storage-access-framework-set-last-modified-date-of-local-documentfile
    @SuppressWarnings("UnusedReturnValue")
    public boolean setLastModified(long time)
    {
        return false;
    }

    public FileInputStream getFileInputStream()
    {
        Uri uri = f.getUri();
        try
        {
            // We cannot use the try-with-resources method, because this would
            // automatically close 'pfd', but we must return an opened one.
            //noinspection resource
            ParcelFileDescriptor pfd = contentResolver.openFileDescriptor(uri, "r");
            if (pfd != null)
            {
                FileDescriptor fd = pfd.getFileDescriptor();
                if (fd != null)
                {
                     return new FileInputStream(pfd.getFileDescriptor());
                }
            }
        } catch (Exception e)
        {
            // ignore
        }
        return null;
    }

    public long created()
    {
        return 0;   // unsupported
    }

    public boolean setCreated(long time)
    {
        return false;   // unsupported
    }

    public FileOutputStream getFileOutputStream()
    {
        Uri uri = f.getUri();
        try
        {
            // We cannot use the try-with-resources method, because this would
            // automatically close 'pfd', but we must return an opened one.
            //noinspection resource
            ParcelFileDescriptor pfd = contentResolver.openFileDescriptor(uri, "rw");
            if (pfd != null)
            {
                FileDescriptor fd = pfd.getFileDescriptor();
                if (fd != null)
                {
                    return new FileOutputStream(pfd.getFileDescriptor());
                }
            }
        } catch (Exception e)
        {
            // ignore
        }
        return null;
    }

    public Path toPath()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            return Paths.get(getPath());
        }
        else
        {
            return null;
        }
    }

    //
    // utility functions
    //

    public boolean existsInSameDirectory(String name)
    {
        DocumentFile parentFile = f.getParentFile();
        if (parentFile != null)
        {
            DocumentFile fn = parentFile.findFile(name);
            if (fn != null)
            {
                return fn.exists();
            }
        }
        return false;
    }

    public String getBaseName()
    {
        String name = getName();
        int index = name.lastIndexOf('.');
        if (index > 0)
        {
            return name.substring(0, index);
        }

        return name;
    }

    /*
     * Check if the new file can be renamed to the original filename.
     * If not, a copy and delete sequence must be used.
     */
    private boolean checkIfRenameable(MyFile newFile)
    {
        DocumentFile thisParent = f.getParentFile();
        boolean isRenameable = (thisParent != null) && (this.getClass().equals(newFile.getClass()));
        if (isRenameable)
        {
            MyFileUsingDocumentFile newDocFile = (MyFileUsingDocumentFile) newFile;
            DocumentFile newParent = newDocFile.f.getParentFile();
            return thisParent == newParent;
        }
        return false;
    }

    /*
     * Replace the object with the new file by using copy or rename or move
     */
    public boolean replaceWith(MyFile newFile)
    {
        DocumentFile thisParent = f.getParentFile();
        String originalName = getName();

        if (checkIfRenameable(newFile))
        {
            /*
             * file is renameable
             */

            // get original name without extension
            String originalBaseName = getBaseName();

            // check for possible backup filename in same directory
            int count = 0;
            String backupFileName = originalBaseName + ".old";
            while (existsInSameDirectory(backupFileName))
            {
                count++;
                backupFileName = originalBaseName + ".old" + count;
            }

            // Backup filename of "bla.mp3" is now "bla.old" or "bla.old1", "bla.old2" etc.
            // Now rename original file to backup filename
            boolean bRes = renameTo(backupFileName);
            if (!bRes)
            {
                return false;
            }

            // rename new file to original file
            bRes = newFile.renameTo(originalName);
            if (!bRes)
            {
                // first re-rename original file to original filename
                renameTo(originalName);  // TODO: result is ignored
                // then give up
                return false;
            }

            // delete the original file, that had been renamed to ".old"
            this.delete();  // TODO: result is ignored
        }
        else
        {
            /*
             * file is NOT renameable
             */

            Log.d(LOG_TAG, "replaceWith() : copy file content");
            OutputStream os = null;
            Uri uri = f.getUri();
            // mode "wt" is unsupported, but "w" behaves like "wt" for Google Drive based files
            try
            {
                os = contentResolver.openOutputStream(uri, "w");
            } catch (FileNotFoundException e)
            {
                Log.e(LOG_TAG, "replaceWith() : exception = " + e.getMessage());
            }
            if (os == null)
            {
                return false;
            }
            if (!Utility.copyFileFromTo(newFile.getFileInputStream(), os))
            {
                return false;
            }
        }

        // open new file, that now has new name
        if (thisParent != null)
        {
            // reopen the freshly renamed or written file, with the original and now current name
            f = thisParent.findFile(originalName);
        }
        else
        {
            Log.e(LOG_TAG, "replaceWith() : cannot re-open file with new name due to invalid parent");
        }
        return true;
    }


    public Uri getUri() {return f.getUri();}

    //
    // not allowed
    //

    /*
    File getFile()
    {
        return f;
    }
     */
}
