/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import androidx.documentfile.provider.DocumentFile;
import android.util.Log;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.MyFile;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.audio.mp4.Mp4AudioHeader;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.KeyNotFoundException;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagField;
import org.jaudiotagger.tag.id3.AbstractTagFrame;
import org.jaudiotagger.tag.id3.AbstractTagFrameBody;
import org.jaudiotagger.tag.id3.ID3v11Tag;
import org.jaudiotagger.tag.id3.ID3v1Tag;
import org.jaudiotagger.tag.id3.ID3v23Tag;
import org.jaudiotagger.tag.id3.framebody.FrameBodyCOMM;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * wrapper for Jaudiotagger
 *
 * References:
 *
 *  http://help.mp3tag.de/main_tags.html
 *  http://www.jthink.net/jaudiotagger/tagmapping.html
 *
 * @noinspection JavadocBlankLines, RedundantSuppression, JavadocLinkAsPlainText, CommentedOutCode
 */
@SuppressWarnings("WeakerAccess")
public class AudioFileInfo
{
    static private final String LOG_TAG = "CMT : AudioFileInfo";
    public String mPath = null;
    public DirectoryTree.DirectoryEntry mDirEntry;
    public DirectoryTree.DirectoryEntry mNewDirEntry = null;   // null or new entry after a rename
    ContentResolver mContentResolver;
    private AudioFile mf = null;
    private MP3File mf3 = null;                         // mp3: same as mf, otherwise null
    private String mp4Brand = null;                     // mp4: if "dash", then write protect, as not supported
    private AudioHeader mAudioHeader = null;
    private Tag mTagsInFile = null;
    private AudioTags mAudioTags = null;

    public static int noOfFileNotFoundErrors = 0;
    public static int noOfCannotReadErrors = 0;
    public static int noOfSuccess = 0;
    public enum errCode
    {
        eOk, eNotFound, eCannotReadFile, eCannotReadTag, eWriteProtected, eGeneralError
    }

    // define tag indices be able to process tags in a loop
    public static final int idAlbum = 0;                // mp3: TALB    mp4: ©alb       flac: ALBUM
    public static final int idArtist = 1;               // mp3: TPE1    mp4: ©ART       flac: ARTIST
    public static final int idAlbumArtist = 2;          // mp3: TPE2    mp4: aART       flac: ALBUMARTIST
    public static final int idConductor = 3;            // mp3: TPE3    mp4: com.apple.iTunes.CONDUCTOR     (should be ©con?)
    public static final int idGrouping = 4;             // mp3: TIT1    mp4: ©grp       flac: GROUPING
    public static final int idTitle = 5;                // mp3: TIT2    mp4: ©nam       flac: TITLE
    public static final int idSubtitle = 6;             // mp3: TIT3    mp4: com.apple.iTunes.SUBTITLE      (should be desc)
    public static final int idComposer = 7;             // mp3: TCOM    mp4: ©wrt       flac: COMPOSER
    public static final int idDiscNo = 8;               // mp3: TPOS    mp4: disk       flac: DISCNUMBER
    public static final int idDiscTotal = 9;            // mp3: TPOS    mp4: disk
    public static final int idTrack = 10;               // mp3: TRCK    mp4: trkn       flac: TRACKNUMBER
    public static final int idTrackTotal = 11;          // mp3: TRCK    mp4: trkn
    public static final int idGenre = 12;               // mp3: TCON    mp4: ©gen       flac: GENRE
    public static final int idYear = 13;                // mp3: TYER    mp4: ©day       flac: DATE
    public static final int idComment = 14;             // mp3: COMM    mp4: ©cmt       flac: COMMENT
    // iTunes:
    public static final int idAppleMovement = 15;       // mp3: MVNM   mp4: ©mvn   flac: MOVEMENT
    public static final int idAppleMovementNo = 16;     // mp3: MVIN   mp4: ©mvi   flac: MOVEMENT_NO
    public static final int idAppleMovementTotal = 17;  // mp3: MVIN   mp4: ©mvc   flac: MOVEMENT_TOTAL
    public static final int idAppleMp3Work = 18;        // mp3: GRP1   mp4: <-->
    public static final int idAppleMp4Work = 19;        // mp3: <-->   mp4: ©wrk   flac: WORK
    // derived fields
    public static final int idAppleWork = 20;           // mp3: GRP1   mp4: ©wrk   flac: WORK
    public static final int idCombinedMovement = 21;    // combine movement or title information
    // number of fields
    public static final int idNum = 22;

    private static final FieldKey[] audioTagIds =
    {
        FieldKey.ALBUM,
        FieldKey.ARTIST,
        FieldKey.ALBUM_ARTIST,
        FieldKey.CONDUCTOR,
        FieldKey.GROUPING,          // 4
        FieldKey.TITLE,
        FieldKey.SUBTITLE,
        FieldKey.COMPOSER,
        FieldKey.DISC_NO,
        FieldKey.DISC_TOTAL,
        FieldKey.TRACK,
        FieldKey.TRACK_TOTAL,
        FieldKey.GENRE,
        FieldKey.YEAR,
        FieldKey.COMMENT,
        // iTunes:
        FieldKey.MOVEMENT,          // 15
        FieldKey.MOVEMENT_NO,       // 16
        FieldKey.MOVEMENT_TOTAL,    // 17
        FieldKey.ITUNES_GROUPING,   // 18
        FieldKey.WORK               // 19
    };


    /* ***********************************************************************************
     *
     * helper to make a backup file
     *
     ***********************************************************************************/
    /*
    private static void copyFile(File source, File dest) throws IOException
    {
        InputStream is = null;
        OutputStream os = null;
        try
        {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[4096];
            int length;
            while ((length = is.read(buffer)) > 0)
            {
                os.write(buffer, 0, length);
            }
        }
        finally
        {
            if (is != null)
                is.close();
            if (os != null)
                os.close();
        }
    }
    */


    /************************************************************************************
     *
     * helper class representing an array of all tag names
     *
     * @noinspection JavadocBlankLines, RedundantSuppression, JavadocLinkAsPlainText
     *
     **********************************************************************************/
    public static class AudioTags
    {
        public String[] tags = new String[idNum];       // the value to be edited
        //public int nvalues[] = new int[idNum];          // number of values for this tag

        public AudioTags copy()
        {
            AudioTags newTags = new AudioTags();
            System.arraycopy(tags, 0, newTags.tags, 0, tags.length);
            return newTags;
        }


        /************************************************************************************
         *
         * combine movement name of form:
         *
         *  "movement" or
         *  "n movement" or
         *  "n/m movement"
         *
         ***********************************************************************************/
        @SuppressWarnings("WeakerAccess")
        public void combineMovement()
        {
            String result;

            String n = tags[idAppleMovementNo];
            String total = tags[idAppleMovementTotal];
            String name = tags[idAppleMovement];
            if (name.isEmpty())
            {
                name = tags[idTitle];
            }
            if (n.isEmpty())
            {
                result = name;
            }
            else
            if (total.isEmpty())
            {
                result = n + " " + name;
            }
            else
            {
                result = n + "/" + total + " " + name;
            }

            tags[idCombinedMovement] = result;
        }


        @SuppressWarnings("BooleanMethodIsAlwaysInverted")
        private boolean isNumber(char c)
        {
            return (c >= '0' && c <= '9');
        }


        /************************************************************************************
         *
         * un-combine movement name of form:
         *
         *  "movement" or
         *  "n movement" or
         *  "n/m movement"
         *
         ***********************************************************************************/
        @SuppressWarnings({"StringConcatenationInLoop"})
        private void separateCombinedMovement()
        {
            int total = 0;
            char c;
            String num;

            String s = tags[idCombinedMovement].trim();

            // parse leading decimal
            num = "";
            int pos;
            for (pos = 0; pos < s.length(); pos++)
            {
                c = s.charAt(pos);
                if (!isNumber(c))
                {
                    break;
                }
                num += c;
            }

            int n = num.isEmpty() ? 0 : Integer.parseInt(num);
            s = s.substring(pos).trim();
            pos = 0;
            if (s.isEmpty())
            {
                tags[idAppleMovement] = "";
            }
            else
            {
                c = s.charAt(pos);
                if (c == '/')
                {
                    s = s.substring(pos + 1).trim();
                    if (!s.isEmpty())
                    {
                        num = "";
                        for (pos = 0; pos < s.length(); pos++)
                        {
                            c = s.charAt(pos);
                            if (!isNumber(c))
                            {
                                break;
                            }
                            num += c;
                        }
                        total = num.isEmpty() ? 0 : Integer.parseInt(num);
                        s = s.substring(pos).trim();
                    }
                }

                tags[idAppleMovement] = s;
            }

            tags[idAppleMovementNo] = (n > 0) ? ("" + n) : ("");
            tags[idAppleMovementTotal] = (total > 0) ? ("" + total) : ("");

            Log.d(LOG_TAG, "separate movement: " + tags[idCombinedMovement]);
            Log.d(LOG_TAG, " -> n     = " + tags[idAppleMovementNo]);
            Log.d(LOG_TAG, " -> total = " + tags[idAppleMovementTotal]);
            Log.d(LOG_TAG, " -> name  = " + tags[idAppleMovement]);
        }


        /************************************************************************************
         *
         * normalise title and movement tags
         *
         * mode = 1: store movement information in separate tags and set title to
         *           movement name with number.
         * mode = 2: same, but remove movement tags
         * mode = 3: like 2, but use proprietary mp3 work tag GRP1 instead of TIT1
         *
         ***********************************************************************************/
        public void normaliseTitleAndMovement(int mode)
        {
            separateCombinedMovement();
            if (tags[idAppleMovementNo].isEmpty())
            {
                tags[idTitle] = tags[idAppleMovement];
            }
            else
            {
                tags[idTitle] = tags[idAppleMovementNo] + ". " + tags[idAppleMovement];
            }

            if (mode == 2)
            {
                //
                // classical music mode
                //

                tags[idAppleWork] = null;                   // do not copy to mp3work and mp4work
                tags[idAppleMp3Work] = "";                  // remove mp3:GRP1
                tags[idAppleMp4Work] = tags[idGrouping];    // use mp4:©wrk
            }
            else
            if (mode == 1)
            {
                //
                // standard mode: no movements, use standard work tag
                //

                tags[idAppleMovementNo] = "";
                tags[idAppleMovementTotal] = "";
                tags[idAppleMovement] = "";
                tags[idAppleWork] = "";         // will be copied to mp3work and mp4work before writing
            }
            else
            {
                //
                // iTunes nonsense mode: no movements, use proprietary work tag for mp3
                //

                tags[idAppleMovementNo] = "";
                tags[idAppleMovementTotal] = "";
                tags[idAppleMovement] = "";
                tags[idAppleWork] = null;                   // do not copy to mp3work and mp4work
                tags[idAppleMp3Work] = tags[idGrouping];    // use proprietary mp3:GRP1  (iTunes bug)
                tags[idAppleMp4Work] = "";                  // do not use mp4:©wrk
            }
        }

    }

    /************************************************************************************
     *
     * file information, not tags
     *
     ***********************************************************************************/
    public static class Info
    {
        public String absPath;
        public String type;
        public String format;
        public long bitRateInKbitPerSecond;
        public int bitsPerSample;
        public int channels;                    // number of channels (1 = mono, 2 = stereo)
        public String strChannels;              // number of channels as text, e.g. "Joint Stereo"
        public boolean lossless;
        public int durationInSeconds;
    }


    /************************************************************************************
     *
     * constructor
     *
     * Opens the file using JaudioTagger.
     * Invalid objects will have a null path.
     *
     ***********************************************************************************/
    public AudioFileInfo(DirectoryTree.DirectoryEntry de, ContentResolver theContentResolver)
    {
        mDirEntry = de;
        mContentResolver = theContentResolver;
    }


    /************************************************************************************
     *
     *  Open an jaudiotagger object with a file
     *
     ***********************************************************************************/
    public errCode open()
    {
        if (!mDirEntry.exists())
        {
            noOfFileNotFoundErrors++;
            Log.e(LOG_TAG, "does not exist: " + mDirEntry.getPath());
            return errCode.eNotFound;
        }
        if (!mDirEntry.canRead())
        {
            noOfCannotReadErrors++;
            Log.e(LOG_TAG, "is not readable: " + mDirEntry.getPath());
            return errCode.eCannotReadFile;
        }
        mPath = mDirEntry.getFilePath();
        errCode err = openInternal(mDirEntry);
        if (err == errCode.eOk)
        {
            readTags();
        }

        return err;
    }


    /************************************************************************************
     *
     *  check for known compatibility
     *
     ***********************************************************************************/
    public boolean isWriteCompatible()
    {
        return (mTagsInFile != null) && ((mp4Brand == null) || (!mp4Brand.equals("dash")));
    }


    /************************************************************************************
     *
     *  Try to open an jaudiotagger object with a file
     *
     ***********************************************************************************/
    private errCode openInternal(DirectoryTree.DirectoryEntry de)
    {
        errCode err;

        try
        {
            MyFile myFile = null;
            Object f = de.getFile();
            if (de instanceof DirectoryTreeFile.DirectoryEntryFile)
                myFile = new MyFileUsingFile((File) f);
            else
            if (de instanceof DirectoryTreeSaf.DirectoryEntrySaf)
                myFile = new MyFileUsingDocumentFile((DocumentFile) f, mContentResolver);

            mf = AudioFileIO.read(myFile);
            err = errCode.eOk;
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "cannot find file " + de.getPath() + " (" + e.getMessage() + ")");
            err = errCode.eNotFound;
        } catch (org.jaudiotagger.audio.exceptions.CannotReadException e)
        {
            Log.e(LOG_TAG, "cannot read audio file " + de.getPath() + " (" + e.getMessage() + ")");
            err = errCode.eCannotReadFile;
        } catch (org.jaudiotagger.audio.exceptions.ReadOnlyFileException e)
        {
            Log.e(LOG_TAG, "cannot open read-only audio file " + de.getPath() + " (" + e.getMessage() + ")");
            err = errCode.eWriteProtected;
        } catch (org.jaudiotagger.tag.TagException e)
        {
            Log.e(LOG_TAG, "cannot read tag from audio file " + de.getPath() + " (" + e.getMessage() + ")");
            err = errCode.eCannotReadTag;
        } catch (org.jaudiotagger.audio.exceptions.InvalidAudioFrameException e)
        {
            Log.e(LOG_TAG, "invalid audio frame in " + de.getPath() + " (" + e.getMessage() + ")");
            err = errCode.eCannotReadFile;
        } /*catch (org.jaudiotagger.audio.exceptions.UnknownFilenameExtensionException e)
        {
            Log.e(LOG_TAG, "unknown file name extension of " + theFile.getPath() + " (" + e.getMessage() + ")");
            if (openAs != null)
            {
                // should not happen here
                mf = null;
            }
        } */ catch (IOException e)
        {
            Log.e(LOG_TAG, "cannot read file " + de.getPath() + " (" + e.getMessage() + ")");
            err = errCode.eCannotReadFile;
        }

        if (err == errCode.eOk)
        {
            mAudioHeader = mf.getAudioHeader();
            if (mAudioHeader != null)
            {
                if (mAudioHeader instanceof Mp4AudioHeader)
                {
                    //Log.d(LOG_TAG, "gotMp4AudioHeader");
                    Mp4AudioHeader theMp4AudioHeader = (Mp4AudioHeader) mAudioHeader;
                    mp4Brand = theMp4AudioHeader.getBrand();
                    Log.d(LOG_TAG, "mp4 brand is \"" + mp4Brand + "\"");
                }
            }
            noOfSuccess++;
        }
        else
        {
            mf = null;
        }

        return err;
    }


    /* ***********************************************************************************
     *
     *  method to convert a movement number to a string with a roman numeral
     *
     ***********************************************************************************/
    /*
    @SuppressWarnings("StringConcatenationInLoop")
    static private String romanNumeral(int n)
    {
        String roman = "";

        if (n >= 40)
        {
            // fallback for large numbers: return arabic number representation
            return roman + n;
        }

        while (n >= 10)
        {
            roman += "X";
            n -= 10;
        }
        while (n >= 9)
        {
            roman += "IX";
            n -= 9;
        }
        while (n >= 5)
        {
            roman += "V";
            n -= 5;
        }
        while (n >= 4)
        {
            roman += "IV";
            n -= 4;
        }
        while (n >= 1)
        {
            roman += "I";
            n -= 1;
        }

        return roman;
    }
    */


    /************************************************************************************
     *
     * get comment tag safely, i.e. without crash
     *
     * special handling to get correct value for id3 and ignore iTunes comments
     *
     ***********************************************************************************/
    static private String getCommentTagStringSafely(Tag tag)
    {
        final FieldKey id = FieldKey.COMMENT;
        String theTagValue;
        try
        {
            theTagValue = null;

            // get multiple values per tag

            List<TagField> theTagFields = tag.getFields(id);
            if (theTagFields != null)
            {
                int n = theTagFields.size();
                Log.w(LOG_TAG, "There are " + n + " values for " + id);

                int i;
                for (i = 0; i < n; i++)
                {
                    TagField theTagField = theTagFields.get(i);
                    if (theTagField.isBinary())
                    {
                        Log.v(LOG_TAG, "id = \"" + theTagField.getId() + "\", value is binary");
                    }
                    else
                    {
                        String theValue;

                        if (theTagField instanceof AbstractTagFrame)
                        {
                            AbstractTagFrameBody theBody = ((AbstractTagFrame) theTagField).getBody();
                            theValue = theBody.getUserFriendlyValue();
                            Log.v(LOG_TAG, "id = \"" + theTagField.getId() + "\", value = \"" + theValue + "\"");

                            if (theBody instanceof FrameBodyCOMM)
                            {
                                FrameBodyCOMM theComment = (FrameBodyCOMM) theBody;
                                String theDescription =  theComment.getDescription();
                                Log.v(LOG_TAG, " This is a comment with description \"" + theDescription + "\"");
                                if ((theDescription != null) && !theDescription.isEmpty())
                                {
                                    Log.v(LOG_TAG, " -> skip this comment");
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            // might be mp4, e.g. Mp4TagTextField
                            theValue = tag.getValue(id, i);
                            Log.v(LOG_TAG, "id = \"" + theTagField.getId() + "\", value = \"" + theValue + "\"");
                        }

                        if (theTagValue == null)
                        {
                            // get first value
                            theTagValue = theValue;
                        }
                    }
                }
            }

            if (theTagValue == null)
            {
                theTagValue = "";
            }
            else
            {
                theTagValue = theTagValue.trim();
            }
        } catch (KeyNotFoundException | UnsupportedOperationException e)
        {
            theTagValue = "";
        }
        return theTagValue;
    }


    /************************************************************************************
     *
     * get tag safely, i.e. without crash
     *
     ***********************************************************************************/
    static private String getTagStringSafely(Tag tag, FieldKey id)
    {
        if (id == FieldKey.COMMENT)
        {
            return getCommentTagStringSafely(tag);
        }

        String theTagValue;
        try
        {
            theTagValue = null;

            // get multiple values per tag

            List<String> theTagValues = tag.getAll(id);
            if (theTagValues != null)
            {
                int n = theTagValues.size();
                Log.w(LOG_TAG, "There are " + n + " values for " + id);

                int i;
                for (i = 0; i < n; i++)
                {
                    String theValue = theTagValues.get(i);
                    Log.v(LOG_TAG, "id = \"" + id + "\", value = \"" + theValue + "\"");

                    if (theTagValue == null)
                    {
                        // get first value
                        theTagValue = theValue;
                    }
                }
            }

            if (theTagValue == null)
            {
                theTagValue = "";
            }
            else
            {
                theTagValue = theTagValue.trim();
            }
        } catch (KeyNotFoundException | UnsupportedOperationException e)
        {
            theTagValue = "";
        }
        return theTagValue;
    }


    /************************************************************************************
     *
     * get all tags from opened file, using JaudioTagger
     *
     ***********************************************************************************/
    private void readTags()
    {
        if (mf == null)
        {
            return;    // file is not open
        }

        Log.v(LOG_TAG, "read meta data for " + mPath);

        mAudioTags = new AudioTags();
        mTagsInFile = mf.getTag();
        if (mTagsInFile == null)
        {
            //BUG: contrary to getTag(), getTagOrCreateAndSetDefault() ignores id3v1 tags
            mTagsInFile = mf.getTagOrCreateAndSetDefault();
        }

        // mp3 id3v1 and id3v11 is suboptimal, convert to id3v23
        if (mf instanceof MP3File)
        {
            mf3 = (MP3File) mf;
            if (mf3.hasID3v1Tag() && !mf3.hasID3v2Tag())
            {
                Log.e(LOG_TAG, "convert ID3v1 tag to ID3v23");
                mTagsInFile = new ID3v23Tag(mf3.getID3v1Tag());
                mf3.setTag(mTagsInFile);    // add new tag
                //mbDirty = true;       // future use
            }
        }

        if (mTagsInFile == null)
        {
            // we should not get here because of ..OrCreateDefault
            Log.e(LOG_TAG, "no audio tags for " + mf.getFile().getAbsolutePath());
            for (int i = 0; i < audioTagIds.length; i++)
            {
                mAudioTags.tags[i] = "";
            }
        }
        else
        {
            for (int i = 0; i < audioTagIds.length; i++)
            {
                mAudioTags.tags[i] = getTagStringSafely(mTagsInFile, audioTagIds[i]);
            }
        }

        //
        // derived fields
        //

        if (!mAudioTags.tags[idAppleMp3Work].isEmpty())
        {
            mAudioTags.tags[idAppleWork] = mAudioTags.tags[idAppleMp3Work];
        }
        else
        if (!mAudioTags.tags[idAppleMp4Work].isEmpty())
        {
            mAudioTags.tags[idAppleWork] = mAudioTags.tags[idAppleMp4Work];
        }
        else
        {
            mAudioTags.tags[idAppleWork] = "";
        }

        mAudioTags.combineMovement();

        Log.v(LOG_TAG, "Grouping     = " + mAudioTags.tags[idGrouping]);
        Log.v(LOG_TAG, "AppleMp3Work = " + mAudioTags.tags[idAppleMp3Work]);
        Log.v(LOG_TAG, "AppleMp4Work = " + mAudioTags.tags[idAppleMp4Work]);
        Log.v(LOG_TAG, "Genre        = " + mAudioTags.tags[idGenre]);

        /*
        Log.v(LOG_TAG, "tagComposer    = " + ret.tagComposer);
        Log.v(LOG_TAG, "tagConductor   = " + ret.tagConductor);
        Log.v(LOG_TAG, "tagSubtitle    = " + ret.tagSubtitle);
        Log.v(LOG_TAG, "tagAlbum       = " + ret.tagAlbum);
        Log.v(LOG_TAG, "tagAlbumArtist = " + ret.tagAlbumArtist);
        Log.v(LOG_TAG, "tagDiscNo      = " + ret.tagDiscNo);
        Log.v(LOG_TAG, "tagDiscNoTotal = " + ret.tagDiscTotal);
        Log.v(LOG_TAG, "tagTrack       = " + ret.tagTrack);
        Log.v(LOG_TAG, "tagTrackTotal  = " + ret.tagTrackTotal);
        Log.v(LOG_TAG, "tagGenre       = " + ret.tagGenre);
        Log.v(LOG_TAG, "=====================================\n\n");
*/

        // on Nexus S with Dalvik: try to force garbage collection here.
        //            (android:largeHeap="true" is not sufficient)
        // This is not necessary with Nexus 5

        System.gc();
    }


    /************************************************************************************
     *
     * get all tags from opened file, using JaudioTagger
     *
     ***********************************************************************************/
    public AudioTags getTags()
    {
        return mAudioTags;
    }


    /**************************************************************************
     *
     * get detailed audio file info, not tags
     *
     *************************************************************************/
    public Info getInfo()
    {
        if (mf == null)
        {
            return null;
        }

        if (mAudioHeader == null)
        {
            return null;
        }
        Log.v(LOG_TAG, "channels = " + mAudioHeader.getChannels());

        Info ret = new Info();
        ret.absPath = mf.getFile().getAbsolutePath();
        ret.type = mAudioHeader.getEncodingType();
        ret.format = mAudioHeader.getFormat();
        ret.bitRateInKbitPerSecond = mAudioHeader.getBitRateAsNumber();
        ret.bitsPerSample = mAudioHeader.getBitsPerSample();
        ret.strChannels = mAudioHeader.getChannels();
        switch(ret.strChannels)
        {
            case "Mono":
                ret.channels = 1;
                break;

            case "2":
                ret.strChannels = "Stereo";
            case "Stereo":
            case "Joint Stereo":
                ret.channels = 2;
                break;

            default:
                try
                {
                    ret.channels = Integer.parseInt(ret.strChannels);
                } catch (NumberFormatException e)
                {
                    ret.channels = 1;
                }
                break;
        }
        ret.lossless = mAudioHeader.isLossless();
        ret.durationInSeconds = mAudioHeader.getTrackLength();

        return ret;
    }


    /************************************************************************************
     *
     * backup audio file, return false in case of failure
     *
     ***********************************************************************************/
    private boolean createBackupFile(ContentResolver theContentResolver)
    {
        final String originalName = mDirEntry.getName();
        final String backName = originalName + ".backup";

        DirectoryTree.DirectoryEntry.opResult res = mDirEntry.existsInSameDirectory(backName);
        if (res == DirectoryTree.DirectoryEntry.opResult.YES)
        {
            // nothing to do
            return true;
        }
        else
        if (res == DirectoryTree.DirectoryEntry.opResult.NO)
        {
            Log.d(LOG_TAG, "going to rename file \"" + originalName + "\" to \"" + backName + "\"");
            if (mDirEntry.renameToEx(backName, true))
            {
                Log.d(LOG_TAG, "renamed file now has the name \"" + mDirEntry.getName() + "\"");
                // Original file successfully renamed, mDirEntry now symbolises the new filename.
                // Copy backup file to new file.
                // The new file will have the original name, and it will be returned by the copy function
                DirectoryTree.DirectoryEntry newDirEntry = mDirEntry.copyTo(originalName, theContentResolver);
                if (newDirEntry != null)
                {
                    Log.d(LOG_TAG, "successfully copied \"" + mDirEntry.getName() + "\" to \"" + newDirEntry.getName() + "\"");
                    mNewDirEntry = newDirEntry;     // the new entry, must reload tags
                }
                else
                {
                    // copy operation failed, undo rename
                    Log.d(LOG_TAG, "undo: going to rename file \"" + mDirEntry.getName() + "\" to \"" + originalName + "\"");
                    mDirEntry.renameToEx(originalName, true);
                    return false;
                }
                return true;
            }
            return false;
        }
        else
        {
            Log.e(LOG_TAG, "fatal error while creating backup file");
            return false;
        }
    }


    /************************************************************************************
     *
     * create a backup file if none exists, yet, as preparation for writing tags
     *
     * return value:
     *
     *  0 = OK, either backup done or not necessary
     *  1 = could not create backup
     *  2 = fatal error
     *
     ***********************************************************************************/
    public int doBackup(ContentResolver theContentResolver)
    {
        if ((mf == null) || (mTagsInFile == null) || (mAudioTags == null))
        {
            Log.e(LOG_TAG, "file is not open");
            return 2;    // file is not open
        }

        // make a backup, if not already done
        if (!createBackupFile(theContentResolver))
        {
            Log.e(LOG_TAG, "cannot create backup file");
            return 1;
        }

        // re-open file after after a rename-and-copy operation
        if (mNewDirEntry != null)
        {
            Log.e(LOG_TAG, "reload tags after a file rename-copy operation");
            mDirEntry = mNewDirEntry;
            mNewDirEntry = null;
            mPath = mDirEntry.getFilePath();       // needed for trigger the scanner, TODO: find better place for this info
            openInternal(mDirEntry);
            readTags();
        }

        return 0;
    }


    /************************************************************************************
     *
     * write all changed tags to file
     *
     * return value:
     *
     *  0 = OK
     *  1 = write access error
     *  2 = fatal error
     *
     ***********************************************************************************/
    public int writeTags(final AudioTags newtags, boolean bRemoveId3v1, boolean bDryRun)
    {
        if ((mf == null) || (mTagsInFile == null) || (mAudioTags == null))
        {
            Log.e(LOG_TAG, "file is not open");
            return 2;    // file is not open
        }

        //
        // copy derived tags back to original tags
        //

        if (newtags.tags[idAppleWork] != null)
        {
            newtags.tags[idAppleMp3Work] = newtags.tags[idAppleWork];
            newtags.tags[idAppleMp4Work] = newtags.tags[idAppleWork];
        }

        //
        // write changed tags
        //

        boolean bChanges = false;

        for (int i = 0; i < audioTagIds.length; i++)
        {
            String oldVal = mAudioTags.tags[i];
            if (oldVal == null)
            {
                oldVal = "";
            }
            String val = newtags.tags[i];
            if (val != null)
            {
                val = val.trim();   // remove leading or trailing spaces
            }
            else
            {
                val = "";
            }

            Log.d(LOG_TAG, "tag " + i + " was " + oldVal);
            Log.d(LOG_TAG, "tag " + i + " is  " + val);

            // skip "≠" entries and unchanged entries
            if (!val.equals("≠") && !val.equals(oldVal))
            {
                try
                {
                    // special handling of comment, as we must preserve iTunes comments
                    if (i == idComment)
                    {
                        // do not delete the field, because this would remove the iTunes COMM tags
                        mTagsInFile.setField(audioTagIds[i], val);      // this might be an empty string
                    }
                    else
                    {
                        if (val.isEmpty())
                        {
                            Log.d(LOG_TAG, "going to delete tag " + i + " from " + mPath);
                            mTagsInFile.deleteField(audioTagIds[i]);
                        } else
                        {
                            Log.d(LOG_TAG, "going to write tag " + i + " to " + mPath);
                            Log.d(LOG_TAG, " tag value: " + val);
                            // delete all existing fields for this tag, necessary if there is more than one
                            mTagsInFile.deleteField(audioTagIds[i]);
                            // create one new field
                            mTagsInFile.setField(audioTagIds[i], val);
                        }
                    }
                    bChanges = true;
                }
                catch (KeyNotFoundException e)
                {
                    Log.w(LOG_TAG, "ignore KeyNotFoundException (" + e.getMessage() + ")");
                } catch (FieldDataInvalidException e)
                {
                    Log.e(LOG_TAG, "ignoreFieldDataInvalidException (" + e.getMessage() + ")");
                }
            }
        }

        if (bChanges && !bDryRun)
        {
            // update old id3v1 format, if necessary
            if (mf3 != null)
            {
                ID3v1Tag theV1Tag = mf3.getID3v1Tag();   // this can be ID3v11Tag
                if (theV1Tag != null)
                {
                    if (bRemoveId3v1)
                    {
                        Log.v(LOG_TAG, "remove redundant ID3v1(1) tag");
                        theV1Tag = null;      // remove old tag
                    } else
                    {
                        // we have both tag formats. Update old format, if desired
                        if (theV1Tag instanceof ID3v11Tag)
                        {
                            Log.v(LOG_TAG, "update redundant ID3v11 tag");
                            theV1Tag = new ID3v11Tag(mf3.getID3v2Tag());
                        }
                        else
                        {
                            Log.v(LOG_TAG, "update redundant ID3v1 tag");
                            theV1Tag = new ID3v1Tag(mf3.getID3v2Tag());
                        }
                    }
                    mf3.setID3v1Tag(theV1Tag);
                }
            }


            try
            {
                mf.commit();
                Log.d(LOG_TAG, "successfully wrote tags to " + mPath);
                return 0;        // successfully written
            } catch (CannotWriteException e)
            {
                Log.e(LOG_TAG, "CannotWriteException (" + e.getMessage() + ")");
                return 2;       // failure
            }
        }
        else
        {
            // no changes, so this function was successful
            return 0;
        }
    }
}
