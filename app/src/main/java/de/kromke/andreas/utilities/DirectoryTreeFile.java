/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * helper class to handle directories
 */

public class DirectoryTreeFile implements DirectoryTree
{
    static public class DirectoryEntryFile implements DirectoryEntry
    {
        File f;

        @SuppressWarnings("WeakerAccess")
        public DirectoryEntryFile(File f)
        {
            this.f = f;
        }

        public DirectoryEntryFile(String path)
        {
            f = new File(path);
        }

        public int compareTo(DirectoryEntry o)
        {
            DirectoryEntryFile to = (DirectoryEntryFile) o;
            return f.compareTo(to.f);
        }

        public boolean isDirectory()
        {
            return f.isDirectory();
        }

        public boolean exists() { return f.exists(); }

        public boolean delete() { return f.delete(); }

        public boolean canRead()
        {
            return f.canRead();
        }

        public boolean canWrite()
        {
            return f.canWrite();
        }

        public boolean copyTo(DirectoryEntry destination, ContentResolver theContenResolver)
        {
            InputStream is;
            try
            {
                is = new FileInputStream(this.f);
            } catch (FileNotFoundException e)
            {
                Log.e(LOG_TAG, "source file not found");
                return false;
            }

            OutputStream os;
            try
            {
                os = new FileOutputStream(((DirectoryEntryFile) destination).f);
            } catch (FileNotFoundException e)
            {
                Log.e(LOG_TAG, "destination file not found");
                Utility.closeStream(is);
                return false;
            }

            return Utility.copyFileFromTo(is, os);
        }

        public DirectoryEntry copyTo(String name, ContentResolver theContentResolver)
        {
            String parentPath = f.getParent();
            File fn = new File(parentPath, name);
            DirectoryEntryFile dfn = new DirectoryEntryFile(fn);
            if (copyTo(dfn, theContentResolver))
            {
                return dfn;
            }
            return null;
        }

        public String getName()
        {
            return f.getName();
        }

        public String getPath()
        {
            return f.getPath();
        }

        public String getFilePath()
        {
            return f.getPath();
        }

        @NonNull
        public DirectoryEntry getParent()
        {
            return new DirectoryEntryFile(f.getParent());
        }

        public Uri getUri()
        {
            return null;
        }

        public File getFile()
        {
            return f;
        }

        @NonNull
        public String toString()
        {
            return (f == null) ? "" : f.getPath();
        }

        public opResult existsInSameDirectory(String name)
        {
            String parentPath = f.getParent();
            File fn = new File(parentPath, name);
            return fn.exists() ? opResult.YES : opResult.NO;
        }

        public boolean renameToEx(String name, boolean changeTo)
        {
            File fp = f.getParentFile();
            if (fp != null)
            {
                File fpn = new File(fp, name);
                if (f.renameTo(fpn))
                {
                    if (changeTo)
                    {
                        f = fpn;
                    }
                    return true;
                }
            }
            return false;
        }

        public opResult isSeekable(ContentResolver contentResolver)
        {
            try(FileInputStream is = new FileInputStream(this.f))
            {
                FileChannel ifc = is.getChannel();
                long pos;
                pos = ifc.position();
                if (pos < 0)
                {
                    return opResult.NO;
                }
                ifc.position(pos);
                return opResult.YES;
            } catch (FileNotFoundException ex)
            {
                Log.e(LOG_TAG, "isSeekable() : file-not-found exception with message = " + ex.getMessage());
                return opResult.ERROR;
            } catch (IOException ex)
            {
                Log.e(LOG_TAG, "isSeekable() : seek unsupported");
                return opResult.NO;
            }
        }
    }


    private static final String LOG_TAG = "CMT : DEF"; //for debugging purposes.
    public static String strWriteProtected = null;
    public static String strNoAccess = null;
    public static boolean mFileReadAccess = false;
    public static boolean mFileWriteAccess = false;
    private File mCurrentDir;       // current location
    private final File mRootDir;

    // constructor
    public DirectoryTreeFile(String basePath)
    {
        File f = new File(basePath);
        if (!f.canRead())
        {
            Log.e(LOG_TAG, "base path not readable:" + basePath);
            basePath = "/";
        }

        mCurrentDir = new File(basePath);
        mRootDir = mCurrentDir;
    }

    public boolean isValid()
    {
        return (mCurrentDir != null);
    }

    public DirectoryTree setCurrent(Context context, DirectoryEntry base)
    {
        mCurrentDir = ((DirectoryEntryFile) base).f;
        return this;
    }

    public boolean isRoot()
    {
        //noinspection SimplifiableIfStatement
        if ((mRootDir == null) || (mCurrentDir == null))
        {
            return true;
        }
        return mRootDir.getPath().equals(mCurrentDir.getPath());
    }

    //get the current directory
    public DirectoryEntry getCurrent()
    {
        return new DirectoryEntryFile(mCurrentDir);
    }

    //get the current path as Uri string
    public String getCurrentUriAsString()
    {
        final String primaryPath = "/storage/emulated/0";
        final String primaryPrefix = "content://com.android.externalstorage.documents/document/primary%3A";
        final String genericPath = "/storage/";
        final String genericPrefix = "content://com.android.externalstorage.documents/document/";
        if (mCurrentDir == null)
            return null;
        String path = mCurrentDir.getPath();
        if (path.startsWith(primaryPath))
        {
            String pathPostfix = path.substring(primaryPath.length());
            if (pathPostfix.startsWith("/"))
            {
                pathPostfix = pathPostfix.substring(1);
            }
            return primaryPrefix + pathPostfix;
        }
        else
        if (path.startsWith(genericPath))
        {
            path = path.substring(genericPath.length());
            int index = path.indexOf('/');
            if (index < 0)
            {
                return genericPrefix + path + "%3A";
            }
            return genericPrefix + path.substring(0, index) + "%3A" + path.substring(index + 1);
        }
        return null;
    }

    //set and get previous directory
    public DirectoryEntry gotoParent()
    {
        String path = mCurrentDir.getPath();
        int p = path.lastIndexOf('/');
        if (p < 0)
        {
            return null;
        }
        path = path.substring(0, p);
        if (path.isEmpty())
        {
            path = "/";
        }
        setCurrent(null, path);
        return getCurrent();
    }

    //set the current directory
    public void setCurrent(Context context, String currentPath)
    {
        mCurrentDir = new File(currentPath);
    }

    //Returns a sorted list of all dirs and files in a given directory.
    public List<DirectoryEntry> getChildren()
    {
        /* directories and files will be sorted separately, and later files will be placed after directories. */
        List<DirectoryEntry> dirs = new ArrayList<>();
        List<DirectoryEntry> files = new ArrayList<>();

        File[] allFiles = mCurrentDir.listFiles();
        if (allFiles != null)
        {
            for (File file : allFiles)
            {
                if (file.isDirectory())
                {
                    dirs.add(new DirectoryEntryFile(file));
                } else
                {
                    files.add(new DirectoryEntryFile(file));
                }
            }

            Collections.sort(dirs);
            Collections.sort(files);
        }

        /* Both lists are sorted, so I can just add the files to the dirs list.
          This will give me a list of dirs on top and files on bottom. */
        dirs.addAll(files);

        return dirs;
    }

    @NonNull
    public String toString()
    {
        return mCurrentDir.getPath();
    }

    @NonNull public String getInfoString()
    {
        String status = "";
        if (!mFileReadAccess)
        {
            status = " (" + strNoAccess + ")";
        }
        else
        if (!mFileWriteAccess || !mCurrentDir.canWrite())
        {
            status = " (" + strWriteProtected + ")";
        }

        return mCurrentDir.getPath() + status;
    }
}
