/*
 * Copyright (C) 2020-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import androidx.documentfile.provider.DocumentFile;
import android.util.Log;

import org.jaudiotagger.audio.MyRandomAccessFile;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;


// https://stackoverflow.com/questions/28897329/documentfile-randomaccessfile

public class MyRandomAccessFileUsingDocumentFile implements MyRandomAccessFile
{
    private static final String LOG_TAG = "CMT : RAD"; //for debugging purposes.
    private ParcelFileDescriptor pfd = null;
    private FileInputStream is = null;
    private FileOutputStream os = null;
    private FileChannel ifc = null;
    private FileChannel ofc = null;
    private boolean bRead;
    private boolean bWrite;

    //
    // constructors
    //

    MyRandomAccessFileUsingDocumentFile(DocumentFile df, final String mode, ContentResolver contentResolver)
    {
        switch (mode)
        {
            case "r":
                bRead = true;
                bWrite = false;
                break;
            case "w":
                bRead = false;
                bWrite = true;
                break;
            case "rw":
                bRead = true;
                bWrite = true;
                break;
        }

        Uri uri = df.getUri();
        try
        {
            pfd = contentResolver.openFileDescriptor(uri, mode);
            if (pfd != null)
            {
                FileDescriptor fd = pfd.getFileDescriptor();
                if (fd != null)
                {
                    if (bRead)
                    {
                        is = new FileInputStream(pfd.getFileDescriptor());
                        ifc = is.getChannel();
                    }
                    if (bWrite)
                    {
                        os = new FileOutputStream(pfd.getFileDescriptor());
                        ofc = os.getChannel();
                    }
                }
            }
        } catch (Exception e)
        {
            // typical exception: "unsupported operation r/w"
            Log.e(LOG_TAG, "MyRandomAccessFileUsingDocumentFile() : exception = " + e.getMessage());
            is = null;
            ifc = null;
            os = null;
            ofc = null;
        }
    }

    public int read() throws IOException
    {
        if (is != null)
        {
            return is.read();
        }
        else
        {
            return -1;
        }
    }

    public int read(byte[] b)
    {
        if (is != null)
        {
            try
            {
                return is.read(b);
            } catch (IOException e)
            {
                // ignore
                Log.e(LOG_TAG, "read(byte[]) : exception : " + e);
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }

    public final byte readByte()
    {
        if (is != null)
        {
            try
            {
                byte[] b = new byte[1];
                int ret = is.read(b);
                return (ret == 1) ? b[0] : 0;
            } catch (IOException e)
            {
                // ignore
                Log.e(LOG_TAG, "readByte() : exception : " + e);
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }

    public final void readFully(byte[] b) throws IOException
    {
        if (is != null)
        {
            int res = is.read(b);
            if (res != b.length)
            {
                throw new IOException();
            }
        }
    }

    public final void readFully (byte[] b, int off, int len) throws IOException
    {
        if (is != null)
        {
            int res = is.read(b, off, len);
            if (res != b.length)
            {
                throw new IOException();
            }
        }
    }

    public final int read(byte[] b, int off, int len)
    {
        if (is != null)
        {
            try
            {
                return is.read(b, off, len);
            } catch (IOException e)
            {
                // ignore
                Log.e(LOG_TAG, "read(byte[], int, int) : exception : " + e);
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }

    public void write(byte[] b, int off, int len)
    {
        if (os != null)
        {
            try
            {
                os.write(b, off, len);
            } catch (IOException e)
            {
                // ignore
                Log.e(LOG_TAG, "write(byte[], int, int) : exception : " + e);
            }
        }
    }

    public void write(byte[] b)
    {
        if (os != null)
        {
            try
            {
                os.write(b);
            } catch (IOException e)
            {
                // ignore
                Log.e(LOG_TAG, "write(byte[]) : exception : " + e);
            }
        }
    }

    public void write(int b)
    {
        if (os != null)
        {
            try
            {
                os.write(b);
            } catch (IOException e)
            {
                // ignore
                Log.e(LOG_TAG, "write(int) : exception : " + e);
            }
        }
    }

    public int skipBytes(int n)
    {
        if (is != null)
        {
            try
            {
                return (int) is.skip(n);
            } catch (IOException e)
            {
                // ignore
                Log.e(LOG_TAG, "skipBytes() : exception : " + e);
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }

    public void seek(long pos) throws IOException
    {
        if (is != null)
        {
            ifc.position(pos);
        }
        if (os != null)
        {
            ofc.position(pos);
        }
    }

    public void close()
    {
        try
        {
            if (is != null)
            {
                is.close();
                is = null;
            }
            if (os != null)
            {
                os.close();
                os = null;
            }
            if (pfd != null)
            {
                pfd.close();
                pfd = null;
            }
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "close() : exception : " + e);
            // ignore
        }
    }

    public final FileChannel getReadChannel()
    {
        if (ifc != null)
        {
            return ifc;
        }
        else
        {
            return null;
        }
    }

    public final FileChannel getWriteChannel()
    {
        if (ofc != null)
        {
            return ofc;
        }
        else
        {
            return null;
        }
    }

    public final FileChannel getChannel()
    {
        return ofc;     // TODO: handle r/w
    }

    public long length()
    {
        if (ifc != null)
        {
            try
            {
                return ifc.size();
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "length() : exception : " + e);
                return -1;
            }
        }

        if (ofc != null)
        {
            try
            {
                return ofc.size();
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "length() : exception : " + e);
                return -1;
            }
        }

        return -1;
    }

    public long getFilePointer()
    {
        if (ifc != null)
        {
            try
            {
                return ifc.position();
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "getFilePointer() : exception : " + e);
                return -1;
            }
        }

        if (ofc != null)
        {
            try
            {
                return ofc.position();
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "getFilePointer() : exception : " + e);
                return -1;
            }
        }

        return -1;
    }

    public void setLength(long newLength)
    {
        if (ofc != null)
        {
            try
            {
                ofc.truncate(newLength);
            } catch (IOException e)
            {
                // ignore
                Log.e(LOG_TAG, "setLength() : exception : " + e);
            }
        }
    }


    public boolean isSeekable()
    {
        long pos = getFilePointer();
        if (pos >= 0)
        {
            try
            {
                seek(pos);
                return true;
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "isSeekable() : exception : " + e);
            }
        }
        return false;
    }


    public boolean isValid()
    {
        return (ifc != null) || (ofc != null);
    }


    public boolean isMemBased()
    {
        return false;
    }
}
