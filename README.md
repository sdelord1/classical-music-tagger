# Classical Music Tagger
<img alt="Logo" src="app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" width="80">

A plain audio file metadata editor ("tagger") especially for **classical music**.

As "pop music" is an abbreviation for "popular music", any other music obviously is "unpopular", so this program could also be called *Unpopular Music Tagger*.

***

<a href='https://play.google.com/store/apps/details?id=de.kromke.andreas.musictagger'><img src='public/google-play.png' alt='Get it on Google Play' height=45/></a>
<a href='https://f-droid.org/app/de.kromke.andreas.musictagger'><img src='public/f-droid.png' alt='Get it on F-Droid' height=45 ></a>

***

# Music Organisation

Other music organisation apps are meant for pop music only and deal with "Songs" and "Artists". This is not suitable for classical (baroque, renaissance, ...) music which is organised by composer, work, movements and performers.

Pop music:

Artist | Album  | Song
-------| ------ | -------------
Smith | Heaven  | 1. Love you, babe
 | | 2. Babe, love you
 | | 3. More love, babe, inne wadr
Jones  | Freedom | 1. Babe, I'm free
 | | 2. Free the innrnatt

Classical music needs more metadata ("tags"), especially the *composer tag* and *grouping tag*, the latter is used to group the movements together:

| Album  | Composer  | Work | Performer | Movement
|------- |-----------| -----| --- | ---
| Cello Concertos | Joseph Haydn | Concerto No. 1 | Fred Stone | 1. Allegro
|                 |              |                |            | 2. Adagio 
|                 |              |                |            | 3. Presto 
|                 | Hella Haydn  | Concerto No. 3 | Jean Water | 1. Allegro
|                 |              |                |            | 2. Adagio


For more information see the Google Play Store entry or the offline help text of the application. Also consult the *Unpopular Music Player*.

# Screenshots
<img alt="No" src="public/Screenshot_1.png" width="320">
<img alt="Yes" src="public/Screenshot_2.png" width="320">
<img alt="Yes" src="public/Screenshot_3.png" width="320">
<img alt="Yes" src="public/Screenshot_4.png" width="320">


# Supported

* Standalone mode, ie. with integrated file browser
* Also directly callable from player app (both *Opus 1 Music Player* and *Unpopular Music Player*)
* SAF support, needed for writing to "external" SD cards and to access USB OTG memory and network shares
* Virtual root directory combines local memory and SAF paths, called "URIs"
* Background file operations with "busy" animation to avoid application freezes
* Basic audio player, also supports SAF so that one can hear music from a USB memory stick
* Common and non-common text tags
* Simultaneous similar changes on multiple files, e.g. for composer or work
* "Goto next" shortcut for fast consecutive editing
* Various audio file types (mp3, mp4, flac, ogg, ...)
* Composer and grouping (work, movements)
* Proprietary Apple iTunes tags for classical music
* Built-in autocompletion for genres and more than 220 composer names
* Either remove or maintain redundant (old) ID3v1 tag blocks
* Five female composer names, looking for more...
* Expert or simplified mode
* Migrate Apple tags to standard or vice versa, or leave both
* Preserve original file as backup
* For safety: always work on copy, not on original file
* File revert
* Portrait and landscape orientation
* Small, fast, low system resources
* Genderism (yet imperfect), especially for German civil service

# Not Supported
* mp4-dash files can be read, but not written to
* Changing or deleting embedded images
* SAF providers must provide the "open tree" functionality
* SAF providers must provide seekable files, due to the tagger library

# Permissions Needed
* Read storage
* Write storage

# License

The Classical Music Tagger is licensed according to GPLv3, see LICENSE file.

# External Licenses

**Jaudiotagger library:**  
Source: http://www.jthink.net/jaudiotagger/  
Copyright: paultaylor@jthink.net  
License: http://www.gnu.org/copyleft/lesser.html
